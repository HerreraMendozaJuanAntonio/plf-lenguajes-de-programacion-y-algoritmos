# Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado?

```mermaid

graph TB
    A["¿Que ha cambiado?"] --> B(Sistemas antiguos)
    B --> |Previos| C(Años 90)
    C --> |Tenian| D(Menor potencia)
    C --> |Existian| F(Arquitecturas diferentes)
    C --> |Se usaban| G(Lenguajes)
    G --> |De| H(Bajo nivel)
    H --> |Como| HA(Ensamblador) 
    C --> |Contaban con| I(Recursos limitados)
    C --> J(Programadores)
    J --> |Enfocados| K(Eficiencia)
    K --> |Programas| KA(Duraderos)
    K --> KB(Sin errores)

    A --> M(Sistemas modernos)
    M --> N(Potencia Mayor)
    M --> O(Arquitecturas)
    O --> OA(Similares)
    M --> P(Lenguajes)
    P --> |De|Q(Alto nivel)
    Q --> |Priorizando|QA(Entendimiento)
    M --> R(Programas)
    R --> |Con|S(Errores)
    S --> |Solucionados con|SA(Parches)
```
Referencia Bibliografica:
[https://canal.uned.es/video/5a6f4bdcb1111f082a8b4619](https://canal.uned.es/video/5a6f4bdcb1111f082a8b4619)

# Hª de los algoritmos y de los lenguajes de programación
```plantuml

@startmindmap
* Historia de los algoritmos
**_ ¿Que es?
*** Lista de operaciones
**** Ordenada
**** Finita
**_ A travez de
*** Una entrada
****_ Genera
***** Una salida
**_ Clasificados
*** Razonables o Polinomiales
**** Tiempo de ejecucion
*****_ Crece
****** Despacio
*******_ Cuando el
******** Problema mas grandes
*** No Razonables o Exponenciales
****_ Cualquier cambio genera
***** Crecimiento
******_ En
******* Tiempo de ejecucion
**_ Implementado mediante
*** Lenguajes de programacion
*** Circuitos
*** Aparatos Mecanicos
*** Modelos de computacion
** Paradigmas de programacion
***_ Como lo son
**** Imperativo
***** Lenguaje Fortran
****** Primer lenguaje de alto nivel
**** Funcional
***** Lenguaje LISP
*****_ Enfocado
****** Simplificacion
**** Orientado a Objetos
***** Lenguaje Simula
**** Logico
***** Lenguaje Prolog
@endmindmap
```
Referencia Bibliografica:
[https://canal.uned.es/video/5a6f4c8fb1111f082a8b4b9c](https://canal.uned.es/video/5a6f4c8fb1111f082a8b4b9c)

# Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación

```plantuml
@startmindmap
    * Lenguajes y paradigmas de programación
    ** Computadora
    ***_ Facilita el
    **** Trabajo Intelectual
    ** Lenguajes de programacion
    ***_ Son una
    **** Comunicacion 
    *****_ De forma
    ****** Natural
    *******_ Con
    ******** Las maquinas
    ** Paradigmas de programacion
    ***_ Son una
    **** Aproximacion
    *****_ A la 
    ****** Solucion
    ***_ Surgen por
    **** Fallos
    **** Resolver problemas
    ** Paradigma Estructurado
    ***_ No depende de
    **** Una arquitectura 
    ***_ Usa
    **** Estructuras de datos
    ***** Abstractas
    ***** Control
    **** Lenguajes
    ***** Basic
    ***** Pascal
    ** Paradigma Funcional
    ***_ Utiliza
    **** Lenguaje Matematico
    **** Funciones Matematicas
    **** Recursividad
    *****_ Esto es
    ****** Repetecion de una funcion
    **** Lenguajes
    ***** ML
    ***** Haskell
    ** Paradigma Logico
    ***_ Usa
    **** Expresiones Logicas
    **** Inferencia
    *****_ Para Conseguir una
    ****** Solucion
    **** Recursividad
    **** Lenguajes
    ***** Prolog 

    left side

    ** Programacion Concurrente
    ***_ Brinda
    **** Solucion de problemas
    *****_ De
    ****** Multiples usuarios
    *******_ Al
    ******** Mismo tiempo
    ** Programacion Distribuida
    ***_ Surge para
    **** Comunicar Ordenadores
    *****_ Los programas estan
    ****** Divididos
    *******_ En todos los 
    ******** Equipos de una misma red
    ** Programacion Orientada a objetos
    ***_ Construir
    **** Elementos de software
    *****_ Llamados
    ****** Objetos
    *******_ Para generar 
    ******** Dialogos entre ellos
    ** Programacion de componentes
    ***_ Utilizacion de
    **** Objetos
    **** Reutilizacion de codigo
    *****_ Para obtener una
    ****** Funcionalidad Mayor
    ** Programacion orientada a aspectos
    ***_ Construccion de
    **** Esqueletos o aspectos
    *****_ Se trabajan
    ****** Separadamente
    *******_ Se unen al 
    ******** Final del proyecto
    ** Programacion orientada a agentes software
    ***_ Enfocada en
    **** Inteligencia artificial
    ****_ Utiliza
    ***** Agentes
    ******_ Que son
    ******* Aplicaciones informaticas
    ********_ Que pueden
    ********* Tomar decisiones
    ********* Perciben su entorno
    ********* Buscan on objetivo
    ********* Se comunican con otros agentes
    @endmindmap
```

Referencia Bibliografica
[https://canal.uned.es/video/5a6f4d3cb1111f082a8b4fbc](https://canal.uned.es/video/5a6f4d3cb1111f082a8b4fbc)